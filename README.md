# aureverse_dpl

Reverses the audio stored in an AU file. Same as project aureverse, but in D programming language rather than in C programming language.

# Rationale

This is merely for self-educational purposes.
