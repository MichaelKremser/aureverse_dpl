module main;

import std.stdio;
import std.file;
import std.conv;

uint ubyte_array_to_uint(ubyte[] bytes)
{
    uint final_value = bytes[3] | bytes[2] * 256 | bytes[1] * 65536 | bytes[0] * 16777216;
    return final_value;
}

int main(string[] args)
{
    auto filename = "/tmp/hallo.au";
    auto filename_rev = "/tmp/hallo_rev_dpl.au";
    writefln("Opening file..." ~ filename ~ "\n");

    // Read the whole file to memory
    auto au_file = cast(ubyte[])read(filename);
    auto file_length = au_file.length;
    writeln("read " ~ to!string(file_length) ~ " bytes from " ~ filename);

    // Read magic number (".snd")
    auto magic_number = to!string(cast(char[])au_file[0..4]);
    writeln("magic_number: " ~ magic_number);

    auto data_offset = ubyte_array_to_uint(au_file[4..8]);
    writeln("data_offset: " ~ to!string(data_offset));

    auto data_size = ubyte_array_to_uint(au_file[8..12]);
    writeln("data_size: " ~ to!string(data_size));

    auto coding = ubyte_array_to_uint(au_file[12..16]);
    if (coding != 3)
    {
        writeln("I am sorry, I only support 16 bit linear PCM by now.");
        return 0;
    }
    writeln("coding: " ~ to!string(coding));

    auto sample_rate = ubyte_array_to_uint(au_file[16..20]);
    writeln("sample_rate: " ~ to!string(sample_rate));

    auto channels = ubyte_array_to_uint(au_file[20..24]);
    writeln("channels: " ~ to!string(channels));

    auto reversed_stream = new ubyte[file_length];
    // Copy header
    for (auto ptr = 0; ptr < data_offset; ptr++)
    {
        reversed_stream[ptr] = au_file[ptr];
    }
    std.file.write(filename_rev, reversed_stream); // must be specified with namespace, otherwise compiler throws an error due to duplicate definitions in /usr/lib/gcc/x86_64-linux-gnu/10/include/d/std/file.d and /usr/lib/gcc/x86_64-linux-gnu/10/include/d/std/stdio.d

    return 0;
}
